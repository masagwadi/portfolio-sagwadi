<html>

<head>
    <meta charset="UTF-8">
    <title>Blog Page</title>



    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">

    <link rel="stylesheet" href="css/style.css">

</head>

<body>



<?php include('include/nav.php'); ?>

   <header>

        <div class="banner">

            <img src="img/header002.jpg" alt="">

            <div class="banner__overlay"></div>
            <span class="banner__copy">
                   <!-- <h1>My name is sagwadi, I wanna be old when I grow up. </h1> -->
                   <p>Date: 25/07/2017</p>
                   <p>BY: Sagwadi Maluleke</p>
<!--                   <p>here are some of my thoughts</p>-->

               </span>

        </div>

    </header>


    <!--
   ##########################################################
   ########################## END HEADER #####################
   ##########################################################
-->









<div class="content">

<div class="blog-copy mb20 mt30">

   <h1 class="blog-copy__title">The title goes over here</h1>

   <p class=" ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!</p>
   <br>
   <p class=" ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!</p>
   <br>

   <span class="blog-copy__image">
      <img src="img/002.jpg" alt="">
   </span>

   <p class=" ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!</p>
   <br>
   <p class=" ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!</p>

</div>




<div class="clearfix mb20  myskills myskills--blog-pre">


 <h1 class="myskills__title">Related</h1>

 <p class="myskills__intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quas, recusandae! Dolore id et cumque debitis nesciunt reiciendis at impedit!</p>
 <hr>

 <div class="grid-x4">

    <div class="skill">
         <span class="skill__image"> <img src="why-drupal.jpg" alt="">   </span>
         <h4 class="skill__title">Drupal</h4>
         <p class="skill__description">Lorem ipsum dolor sit amet.</p>
         <!-- <span class="skill__level skill__level--beginer">beginer</span> -->
    </div>

 </div>

 <div class="grid-x4">

    <div class="skill">
         <span class="skill__image"> <img src="img/001.jpg" alt="">   </span>
         <h4 class="skill__title">WordPress</h4>
         <p class="skill__description">Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.</p>
         <!-- <span class="skill__level skill__level--beginer">beginer</span> -->
    </div>

 </div>

 <div class="grid-x4">

    <div class="skill">
         <span class="skill__image"> <img src="img/002.jpg" alt="">   </span>
         <h4 class="skill__title">Laravel</h4>
         <p class="skill__description">Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.</p>
         <!-- <span class="skill__level skill__level--beginer">beginer</span> -->
    </div>

 </div>

 <div class="grid-x4">

    <div class="skill">
         <span class="skill__image"> <img src="img/003.jpg" alt="">   </span>
         <h4 class="skill__title">PHP</h4>
         <p class="skill__description">Lorem ipsum dolor sit amet.</p>
         <!-- <span class="skill__level skill__level--beginer">beginer</span> -->
    </div>

 </div>






</div>




</div>
<!--
   ########################## END OF CONTENT#####################
-->


        <footer>



        </footer>

</body>



<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/masonry.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.js"></script>



<script type="text/javascript" src="js/slick.min.js"></script>


<script type="text/javascript" src="js/main.js"></script>

</html>
